## Hello there! I'm Natalia 👋

I'm a second-year student of Computer Science at the University of Gdańsk and a member of Work In Progress scientific club. My primary interest is web development.

This profile mostly contains my beginner's projects. If you want to see my more advanced work check out my GitHub profile [@n-niewiadowska](https://github.com/n-niewiadowska)!

You can also contact me here: [![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/natalia-niewiadowska-266304290/)
